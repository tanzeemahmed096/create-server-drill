const http = require("http");
const httpServer = require("./src/server/httpServer.js");
const port = process.env.PORT || 3000;

const server = http.createServer(httpServer);

server.listen(port, () => {
    console.log(`Listening to ${port}`);
})