const http = require("http");
const fs = require("fs").promises;
const path = require("path");
const uuid = require("uuid");

function httpServer(request, response) {
  if (request.url === "/") {
    response.writeHead(200, { "Content-Type": "text" });
    response.write("HTTP drills");
    return response.end();
  } else if (request.url === "/html" && request.method === "GET") {
    const filePath = path.join(__dirname, "../data/index.html");
    fs.readFile(filePath)
      .then((data) => {
        response.writeHead(200, { "Content-Type": "text/html" });
        response.write(data);
        return response.end();
      })
      .catch((err) => {
        response.writeHead(500, "Server Error");
        response.write("Something went wrong");
        console.error(err);
        return response.end();
      });
  } else if (request.url === "/json" && request.method === "GET") {
    const filePath = path.join(__dirname, "../data/jsonObj.json");
    fs.readFile(filePath, "utf-8")
      .then((data) => {
        response.writeHead(200, { "Content-Type": "application/json" });
        response.write(data);
        return response.end();
      })
      .catch((err) => {
        response.writeHead(500, "Server Error");
        response.write("Something went wrong");
        console.error(err);
        return response.end();
      });
  } else if (request.url === "/uuid" && request.method === "GET") {
    try {
      const uniqueUuid = {
        uuid: uuid.v4(),
      };
      response.writeHead(200, { "Content-Type": "application/json" });
      response.write(JSON.stringify(uniqueUuid));
      return response.end();
    } catch (err) {
      response.writeHead(500, "Server Error");
      response.write("Something went wrong");
      console.error(err);
      return response.end();
    }
  } else if (request.url.startsWith("/status") && request.method === "GET") {
    try {
      const statusCode = request.url.split("/")[2];
      const statusResult = {};
      statusResult.statusCode = http.STATUS_CODES[statusCode];

      if (JSON.stringify(statusResult) === "{}") {
        throw new Error("Status Code is not provided");
      }

      response.writeHead(200, { "Content-Type": "application/json" });
      response.write(JSON.stringify(statusResult));
      return response.end();
    } catch (err) {
      response.writeHead(400, "Bad Request");
      response.write("Invalid Status Code.");
      console.error(err);
      return response.end();
    }
  } else if (request.url.startsWith("/delay") && request.method === "GET") {
    try {
      const duration = request.url.split("/")[2];
      if (isNaN(duration)) {
        throw new Error("Duration is not a number");
      } else {
        setTimeout(() => {
          response.writeHead(200, { "Content-Type": "application/json" });
          response.write(`Successfull Response with 200 status code`);
          return response.end();
        }, duration * 1000);
      }
    } catch (err) {
      response.writeHead(400, "Bad Request");
      response.write("Error in Delay Duration");
      console.error(err);
      return response.end();
    }
  } else {
    response.writeHead(404, "Bad Request");
    response.write("Page Not Found.");
    return response.end();
  }
}

module.exports = httpServer;